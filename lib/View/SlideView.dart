import 'package:flutter/material.dart';
import 'package:gdi_news/Controller/HomeVC.dart';
import 'package:gdi_news/View/NewsView.dart';
// ignore: must_be_immutable
class SlideView extends StatelessWidget {

  final List<String> titles = ['ទំព័រដើម','នាយកដ្ឋាន','អំពីអគ្គនាយកដ្ឋាន','លិខិតបទដ្ឋានគតិយុត្តិ','ព័ត៌មាន','ការងារយេនឌ័រ','ព័ត៌មានអត្តសញ្ញាណប័ណ្ណ'];
  Function(String name) onTap;
  SlideView(this.onTap);

  Widget build(BuildContext context) {
    return Drawer(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          SizedBox(
            height: 180,
            child: DrawerHeader(
                decoration: BoxDecoration(color: Colors.blue),
                child: Column(
                  children: [
                    Image.asset('assets/images/logo.png',height: 70,width: 70),
                    SizedBox(height: 5),
                    Text('អគ្គនាយកដ្ឋានអត្តសញ្ញាណកម្ម',style: TextStyle(color: Colors.white,fontSize: 18)),
                    Text('General Department of Identification',style: TextStyle(color: Colors.white)),
                  ],
                )
            ),
          ),
          Expanded(
              child: ListView(
                  itemExtent: 50,
                  children: new List.generate(7, (index) => new ListTile(
                    title: Text(
                      this.titles[index],
                      style: TextStyle(fontSize: 14),
                    ),
                    onTap: () => {
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => HomeVC(this.titles[index])),
                    )
                  },
                  )
                  )
              )
          )
        ],
      ),
    );
  }
}