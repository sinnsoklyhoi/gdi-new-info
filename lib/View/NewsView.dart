import 'package:flutter/material.dart';
import 'package:gdi_news/Model/Category.dart';
import 'package:gdi_news/Model/News.dart';

class NewsView extends StatelessWidget {

  final Category category;
  const NewsView({this.category});

  @override
  Widget build(BuildContext context) {
      return Scaffold(
          backgroundColor: Colors.white10,
          body: Container(
            color: Colors.white,
            child: FutureBuilder<List<News>>(
              future: fetchNews('https://news.gdi.gov.kh/wp-json/gdi/v1/menu/news'),
              builder: (context,snapshot) {
                if (snapshot.hasError) print(snapshot.error);
                if (snapshot.hasData) {
                  List<News> news = snapshot.data;
                  return ListView.builder(
                      itemExtent: 180,
                      itemCount: news.length,
                      itemBuilder: (context, index) {
                        return Container(
                            height: MediaQuery.of(context).size.height/4+20,
                            width: MediaQuery.of(context).size.height/4+20,
                            child: _newsCardView(news[index])
                        );
                      }
                  );
                } else {
                  return Center(child: CircularProgressIndicator(),);
                }
              },
            ),
          )
      );
  }

  _newsCardView(News news) {
    return Card(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(0)),
      shadowColor: Colors.white,
      elevation: 0,
      margin: EdgeInsets.symmetric(horizontal:0,vertical: 5),
      child: Container(
        padding: EdgeInsets.all(10),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Expanded(
              child: Container(
                child: Image.network(news.images.first,height:200,width: 60,
                  fit: BoxFit.cover,
                ),
              ),
            ),
            Expanded(
              child: Container(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(left: 10,right: 5),
                      child: Text(news.title,maxLines: 2, style: TextStyle(fontSize: 14,color: Colors.black),),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 5, left: 10,right: 5),
                      child: Text(news.title,maxLines: 3,style: TextStyle(fontSize: 13,color: Colors.grey),),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 5, left: 10),
                      child: Text("01/05/21",style: TextStyle(fontSize: 13,color: Colors.grey),),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}