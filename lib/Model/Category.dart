import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart' as http;

class Category {
  final int id;
  final int dbId;
  final int type;
  final String category;
  final String title;

  Category({this.id,this.dbId,this.type,this.category,this.title});

  factory Category.fromJson(Map<String, dynamic> json) {
    return Category(
      id: json['id'] as int,
      dbId: json['dbId'] as int,
      type: json['type'] as int,
      category: json['object'] as String,
      title: json['title'] as String
    );
  }
}

Future<List<Category>> fetchCategory(String url) async {
  http.Response res = await http.Client().get(Uri.parse(url));
  if (res.statusCode == 200) {
    List<Category> categories = [];
    Map<String, dynamic> parsed = jsonDecode(res.body) as Map<String, dynamic>;
    List<dynamic> data = parsed["data"];
    data.forEach((element) {
      Category category = Category.fromJson(element);
      categories.add(category);
    });
    return categories;
  } else {
    throw Exception("Internet Error : ${res.statusCode}");
  }
}