import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart' as http;

class News {
  final int id;
  final String title;
  final String description;
  final List<String> images;
  final String createDate;

  News({this.id, this.title, this.images, this.description, this.createDate});

  factory News.fromJson(Map<String, dynamic> json) {
    List<String> images = [];
    List<Map<String, dynamic>> objcImages = json['images'] as List<Map<String, dynamic>>;
    print(json);
    objcImages.forEach((obj) {
      String image = obj['thumbnail'];
      images.add(image);
    });

    return News(
        id: json['id'],
        title: json['title'],
        images: images,
        description: json['description'],
        createDate: json['created_date']
    );
  }
}

Future<List<News>> fetchNews(String url) async {
  http.Response res = await http.Client().get(Uri.parse(url));
  if (res.statusCode == 200) {
    Map<String, dynamic> parsed = jsonDecode(res.body) as Map<String, dynamic>;
    List<dynamic> data = parsed["data"];
    List<News> news = data.map((e) => News.fromJson(e)).toList();
    return news;
  } else {
    throw Exception("Internet Error : ${res.statusCode}");
  }
}