import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart' as http;

enum Method {
  POST, GET
}

abstract
class Service {

  Method _method;
  String _url;
  String _params;
  bool _isFetchObjc = false;

  // override for set up server setUrl , setParams , setMethod,..
  void setService() {
    this.setParams(_params);
    this.setMethod(_method);
    this.setUrl(_url);
  }

  void fetch() {
    this.setService();
    if (_isFetchObjc == true) {
      Future<List<Object>> objc = _fetchArrayObject();
      parsedArrayObject(objc);
    } else {
      Future<Object> objc = _fetchObject();
      parsedArrayObject(objc);
    }
  }

  void parsedArrayObject(Future<List<Object>> objc) {

  }

  void parsedObject(Future<List<Object>> objc) {

  }

  Future<Object> _fetchObject() async {
    var fullUrl = 'https://news.gdi.gov.kh/wp-json/gdi/v1/' + this._url;
    if (_params != null) {
      fullUrl += "?" + _params;
    }
    var response = await http.get(
      Uri.parse(fullUrl),
    );
    switch (_method) {
      case Method.POST:
        response = await http.post(
          Uri.parse(fullUrl),
        );
    }

    if (response.statusCode == 200) {
      return jsonDecode(response.body);
    } else {
      throw Exception('Failed to load album');
    }
  }

  Future<List<Object>> _fetchArrayObject() async {
    var fullUrl = 'https://news.gdi.gov.kh/wp-json/gdi/v1/' + this._url;
    if (_params != null) {
      fullUrl += "?" + _params;
    }
    var response = await http.Client().get(
      Uri.parse(fullUrl),
    );
    switch (_method) {
      case Method.POST:
        response = await http.Client().post(
          Uri.parse(fullUrl),
        );
    }

    if (response.statusCode == 200) {
      return jsonDecode(response.body);
    } else {
      throw Exception('Failed to load album');
    }
  }

  setUrl(String url) {
    this._url = url;
  }

  setParams(String params) {
    this._params = params;
  }

  setMethod(Method method) {
    this._method = method;
  }

  setIsFetchArrayObject(bool isArray) {
    this._isFetchObjc = isArray;
  }
}