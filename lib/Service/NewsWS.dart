import 'dart:convert';
import 'package:gdi_news/Model/News.dart';
import 'package:gdi_news/Service/Service.dart';

// ignore: must_be_immutable
class NewsWS extends Service {

  List<News> news = [];

  final int type;
  final int limit;
  final int offset;

  NewsWS({this.type,this.limit,this.offset,this.news});

  @override
  void setService() {
    this.setUrl('menu/news');
    this.setParams('type=' + type.toString() + '&limit=' + limit.toString() + '&offset' + offset.toString());
    this.setMethod(Method.GET);
  }

  @override
  void parsedArrayObject(Future<List<Object>> objc) {
    if (objc as List<News> != null) {
      this.news = objc as List<News>;
    } else {
      print('Parsed Error');
    }
  }
}