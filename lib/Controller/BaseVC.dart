import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:gdi_news/Controller/PostVC.dart';
import 'package:gdi_news/View/ListView.dart';
import 'package:gdi_news/View/SlideView.dart';
import 'package:gdi_news/Extension/StateLessWidget+Ex.dart';
import 'package:gdi_news/Model/News.dart';
import 'package:gdi_news/Service/API.dart';

// ignore: must_be_immutable
class BaseVC extends StatelessWidget {

  final String title;

  BaseVC({this.title});

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
      return Scaffold(
        drawer: SlideView( (title,index) => {
          this._selectedRow(context,title, index)
        }),
        appBar: AppBar(
          title: Text(this.title)
        ),
        body: this.getBody(),
      );
  }

  Widget getBody() {
    return FutureBuilder<List<News>> (
      future: fetchNews(API.fetchPost),
      builder: (context,snapshot) {
        if (snapshot.hasError) print(snapshot.error);
        if (snapshot.data != null) {
          List<News> news = snapshot.data;
          return Scaffold(
            body: BaseListView(news),
          );
        } else {
          return Text('');
        }
      },
    );
  }

  _selectedRow(BuildContext context, String title, int index) {
    if (index == 0) {
      this.nextVC(context,PostVC(title));
    } else if (index == 1) {
      
    }
  }
}