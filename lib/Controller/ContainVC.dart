import 'package:flutter/material.dart';
import 'package:gdi_news/Model/Category.dart';
import 'package:gdi_news/View/NewsView.dart';

class ContainVC extends StatelessWidget {
  final Category category;
  const ContainVC({this.category});

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Center(
        child: NewsView(),
      ),
    );
  }
}