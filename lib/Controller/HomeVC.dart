import 'package:flutter/material.dart';
import 'package:gdi_news/Controller/ContainVC.dart';
import 'package:gdi_news/Model/Category.dart';
import 'package:gdi_news/View/SlideView.dart';

class HomeVC extends StatelessWidget {
  final String name;
  const HomeVC(this.name);
  @override
  Widget build(BuildContext context) {

    return MaterialApp(
      home: FutureBuilder<List<Category>> (
        future: fetchCategory('https://news.gdi.gov.kh/wp-json/gdi/v1/menu/news'),
        builder: (context,snapshot) {
        if (snapshot.hasError) print(snapshot.error);
        if (snapshot.data != null) {
          List<Category> categories = snapshot.data;
          return DefaultTabController(
            length: snapshot.data.length,
            child: Scaffold(
              drawer: SlideView(onTapItem(this.name)),
              appBar: AppBar(
                title: Text(this.name),
                bottom: TabBar(
                    isScrollable: true,
                    tabs: List.generate(categories.length, (index) => Row(
                        children: [
                          Tab(text: categories[index].title,)
                        ]
                    ))
                ),
              ),
              body: TabBarView(
                children: List.generate(categories.length, (index) => ContainVC()),
              ),
            ),
          );
        } else {
          return Text('');
        }
        },
      ),
    );
  }

  onTapItem(String name) {
    print(this.name);
  }
}